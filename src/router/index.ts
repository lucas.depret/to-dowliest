import { Notify } from 'quasar';
import { route } from 'quasar/wrappers';
import VueRouter from 'vue-router';
import { Store } from 'vuex';
import { StateInterface } from '../store';
import routes from './routes';
import firebaseServices from '../services/firebase';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default route<Store<StateInterface>>(({ Vue, store }) => {
  Vue.use(VueRouter);

  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach(async (to, from, next) => {
    const { ensureAuthIsInitialized, isAuthenticated } = firebaseServices;
    try {
      // Force the app to wait until Firebase has
      // finished its initialization, and handle the
      // authentication state of the user properly
      await ensureAuthIsInitialized(store);
      if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (isAuthenticated(store)) {
          next();
        } else {
          next('/');
        }
      } else if ((to.path === '/' && isAuthenticated(store))
        || (to.path === '/' && isAuthenticated(store))) {
        next('/todo');
      } else {
        next();
      }
    } catch (err) {
      console.error(err);
      Notify.create({
        message: `${err}`,
        color: 'negative',
      });
    }
  });

  return Router;
});
