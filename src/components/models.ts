export interface Todo {
  title: string;
  description: string;
  done: string;
  createdAt: Date;
}
