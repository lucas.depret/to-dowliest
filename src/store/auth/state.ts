export interface AuthStateInterface {
  isAuthenticated:boolean;
  isReady: boolean;
  uid:'',
}

function state(): AuthStateInterface {
  return {
    isAuthenticated: false,
    isReady: false,
    uid: '',
  };
}

export default state;
