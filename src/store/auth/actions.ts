import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthStateInterface } from './state';

const actions: ActionTree<AuthStateInterface, StateInterface> = {
  /**
   * Create a new user in application
   *
   * @param  {any} $root - the application root
   * @param  {any} data - email and password of new user
   * @return {Promise} UserCredentials
   */
  createNewUser($root:any, data:any) {
    const { $fb } = (this as any);
    const { email, password } = data;
    return $fb.createUserWithEmail(email, password);
  },
  /**
   * User login in application
   *
   * @param  {any} $root - the application root
   * @param  {any} data - email and password of user
   * @return {Promise} UserCredentials
   */
  loginUser($root:any, payload:any) {
    const { $fb } = (this as any);
    const { email, password } = payload;
    return $fb.loginWithEmail(email, password);
  },
  /**
   * User logout in application
   *
   * @param  {any} $root - the application root
   * @param  {any} data - email and password of user
   */
  async logoutUser() {
    await (this as any).$fb.logoutUser();
  },

  routeUserToAuth() {
    (this as any).$router.push({
      path: '/',
    });
  },
};

export default actions;
