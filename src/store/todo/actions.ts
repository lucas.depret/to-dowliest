import { Todo } from 'src/components/models';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { TodoStateInterface } from './state';

const actions: ActionTree<TodoStateInterface, StateInterface> = {
  /**
   * Add a element in the todolist from firestore
   *
   * @param  {any} context - the application context
   * @param  {Todo} data - the new todo element
   */
  async addTodo(context:any, data:Todo) {
    await (this as any).$fb.userRefUpdateAdd('todos', context.rootState.auth.uid, data);
  },
  /**
   * Delete a element in the todolist from firestore
   *
   * @param  {any} context - the application context
   * @param  {Todo} data - the delete todo element
   */
  async deleteTodo(context:any, data:Todo) {
    const todos = context.rootState.todo.todos.filter((el:Todo) => el.createdAt !== data.createdAt);
    await (this as any).$fb.userRefUpdateChange('todos', context.rootState.auth.uid, todos);
  },
  /**
   * Change the the todolist from firestore
   *
   * @param  {any} context - the application context
   * @param  {Todo} data - array of todo
   */
  async changeTodo(context:any, data:Todo[]) {
    await (this as any).$fb.userRefUpdateChange('todos', context.rootState.auth.uid, data);
  },
};

export default actions;
