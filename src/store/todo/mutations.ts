import { MutationTree } from 'vuex';
import firebase from 'firebase';
import { TodoStateInterface } from './state';
import 'firebase/firestore';

const mutation: MutationTree<TodoStateInterface> = {
  /**
     * set the store with the firestore collection
     *
     * @param  {TodoStateInterface} state - state of doto store
     */
  async setTodoswithFB(state: TodoStateInterface) {
    const todoRef:firebase.firestore.DocumentReference = (this).$fb.userRef('todos', (this).state.auth.uid);
    const get:firebase.firestore.DocumentSnapshot = await todoRef.get();
    const data:any = get.data();
    if (get.exists) state.todos = data.todos;
  },
};

export default mutation;
