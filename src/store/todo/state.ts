import { Todo } from 'src/components/models';

export interface TodoStateInterface {
  todos: Todo[];
}

function state(): TodoStateInterface {
  return {
    todos: [],
  };
}

export default state;
