import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { TodoStateInterface } from './state';

const getters: GetterTree<TodoStateInterface, StateInterface> = {
};

export default getters;
