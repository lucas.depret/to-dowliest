import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { TodoStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const doto: Module<TodoStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default doto;
