import * as base from './base';
import * as email from './email';
import * as db from './db';

export default ({ ...base, ...email, ...db });
