import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

/**
 * Firestore
 * https: //firebase.google.com/docs/reference/js/firebase.firestore.Firestore.html
 *
 * @return {Interface} returns Firestore
 */
export const firestore = () => firebase.firestore();
/**
 * Get the Document Reference of user in relation to specific collection
 *
 * @param  {String} collectionName - Firestore collection name
 * @param  {String} id - Uid to assign to a doc in firestore collection
 * @return {DocumentReference} - Document Reference of user
 */
export const userRef = (collectionName:string, id:string):firebase.firestore.DocumentReference => (
  firestore().collection(collectionName).doc(id)
);
/**
 * Add element in collection of user
 *
 * @param  {String} collectionName - Firestore collection name
 * @param  {String} id - Uid to assign to a doc in firestore collection
 * @param  {String} data - new element to add in collection of user
 */
export const userRefUpdateAdd = async (collectionName:string, id:string, data:any) => {
  const docRef:firebase.firestore.DocumentReference = userRef(collectionName, id);
  const doc:firebase.firestore.DocumentSnapshot = await docRef.get();
  if (!doc.exists) await docRef.set({ [collectionName]: [] });
  await docRef.update({ [collectionName]: firebase.firestore.FieldValue.arrayUnion(data) });
};
/**
 * Change content in collection of user
 *
 * @param  {String} collectionName - Firestore collection name
 * @param  {String} id - Uid to assign to a doc in firestore collection
 * @param  {String} data - new element to remplace content in collection of user
 */
export const userRefUpdateChange = async (collectionName:string, id:string, data:any) => {
  await userRef(collectionName, id).update({ [collectionName]: data });
};
