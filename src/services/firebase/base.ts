/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/ban-types */

import firebase from 'firebase/app';
import 'firebase/auth';

/**
 * Firebase's auth interface method
 * https: //firebase.google.com/docs/reference/js/firebase.auth.html#callable
 * @return {Object} currentUser object from firebase
 */
export const auth = ():firebase.auth.Auth => firebase.auth();

/**
 * Async function providing the application time to
 * wait for firebase to initialize and determine if a
 * user is authenticated or not with only a single observable
 */
export const ensureAuthIsInitialized = (store:any) => {
  if (store.state.auth.isReady) return true;
  // Create the observer only once on init
  return new Promise<void>((resolve, reject) => {
    // Use a promise to make sure that the router will eventually show the route after the auth is initialized.
    const unsubscribe = firebase.auth().onAuthStateChanged(() => {
      resolve();
      unsubscribe();
    }, () => {
      reject(new Error('Looks like there is a problem with the firebase service. Please try again later'));
    });
  });
};

/** Convenience method to initialize firebase app
 * @param  {Object} config
 * @return {firebase.app.App} firebase object
 */
export const fBInit = (config:object): firebase.app.App => firebase.initializeApp(config);

/**
 * @param  {Object} store - Vuex store
 */
export const isAuthenticated = (store:any):boolean => store.state.auth.isAuthenticated;
/** Handle the auth state of the user and
 * set it in the auth store module
 * @param  {Object} store - Vuex Store
 * @param  {Object} currentUser - Firebase currentUser
 */
export const handleOnAuthStateChanged = (store:any, currentUser:any) => {
  const initialAuthState:boolean = isAuthenticated(store);
  store.commit('auth/setAuthState', {
    isAuthenticated: currentUser !== null,
    isReady: true,
    uid: (currentUser ? currentUser.uid : ''),
  });
  if (currentUser) store.commit('todo/setTodoswithFB');
  if (!currentUser && initialAuthState) {
    store.dispatch('auth/routeUserToAuth');
  }
};

/**
 * remove firebase auth token
 */
export const logoutUser = () => auth().signOut();

export default {
  auth, ensureAuthIsInitialized, fBInit, handleOnAuthStateChanged, isAuthenticated, logoutUser,
};
