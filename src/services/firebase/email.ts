import firebase from 'firebase/app';
import 'firebase/auth';

/**
 * Create new user with firebase
 *
 * @param {String} email - A Valid email
 * @param {String} password - Password
 *
 * @return {Promise} UserCredentials
 */
export const createUserWithEmail = (email:string, password:string):
Promise<firebase.auth.UserCredential> => (
  firebase.auth().createUserWithEmailAndPassword(email, password)
);

/**
 * Login user with firebase
 *
 * @param {String} email - A Valid email
 * @param {String} password - Password
 *
 * @return {Promise} UserCredentials
 */
export const loginWithEmail = (email:string, password:string):
  Promise<firebase.auth.UserCredential> => (
  firebase.auth().signInWithEmailAndPassword(email, password)
);
