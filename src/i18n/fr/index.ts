// This is just an example,
// so you can safely delete all default props below

export default {
  login: 'ME CONNECTER',
  loginTitle: 'Me connecter',
  signin: 'CRÉER UN COMPTE',
  signout: 'ME DÉCONNECTER',
  password: 'Mot de passe',
  messageLogin: 'Afin d\'accéder à votre Todolist, merci de bien vouloir vous authentifier.',
  messageLoginDrawer: 'Entrez vos identifiants pour vous connecter',
  hello: 'Bonjour',
  typePassword: 'Veuillez saisir votre mot de passe',
  weakPassword: 'Mot de passe trop faible. Veuillez insérer au moins 8 caractères',
  typeMail: 'Veuillez saisir votre Email',
  wrongMail: 'Email incorrect',
  noneTodo: 'Aucune todo, créez votre premier todo en cliquant sur +',
  addElement: 'Ajout un élément',
  language: 'Langue',
  inputDescription: 'Ajouter une description',
};
