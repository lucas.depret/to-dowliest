// This is just an example,
// so you can safely delete all default props below

export default {
  login: 'LOGIN',
  loginTitle: 'Login',
  signin: 'CREATE ACCOUNT',
  signout: 'SIGNOUT',
  password: 'Password',
  hello: 'Hey',
  messageLogin: 'In order to access your Todolist, please authenticate yourself.',
  messageLoginDrawer: 'Enter your identifiers to connect',
  typePassword: 'Please type your Password',
  weakPassword: 'Password so weak. Please insert at least 8 character',
  typeMail: 'Please type your Email',
  wrongMail: 'Invalid Email',
  noneTodo: 'No todo, create your first todo by clicking on +',
  addElement: 'Add a element',
  language: 'Language',
  inputDescription: 'Add a description',
};
