import enUS from './en-us';
import fr from './fr';

export default {
  'fr': fr,
  'en-us': enUS,
};
