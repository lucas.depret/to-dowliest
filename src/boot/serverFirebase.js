/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import firebaseService from '../services/firebase/index.ts';

export default ({ store }) => {
  const config = process.env.firebaseConfig;
  store.$fb = firebaseService;
  firebaseService.fBInit(config);
  firebaseService.auth().onAuthStateChanged((user) => {
    firebaseService.handleOnAuthStateChanged(store, user);
  }, (error) => {
    console.error(error);
  });
};
