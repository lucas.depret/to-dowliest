import { Selector, Role } from "testcafe";
import { domainPort, url } from '../config/test';

const uriLogin = '/';
const uriHome = '/todo';

const speed = 0.6;

const loginErrorMessage = 'Please input your login or email !';
const passwordErrorMessage = 'Please input your password !';
const loginFailedMessage = 'Authentification failed !';

fixture`Login`.page`${domainPort}${uriLogin}`;

const AccountUser = Role(`${url}${uriLogin}`, async t => {
    await t
        .setTestSpeed(speed)
        .click('#drawer')
        .typeText('.login_mail', 'lucas.depret@eilwo.zyx')
        .typeText('.login_secret', 'Please1coofee')
        .click('#login_submit')
    }
);

  test("Login with no login and password", async t => {
      await t
          .setTestSpeed(speed)
          .click('#drawer')
          .click('#login_submit')
  });

  test("Login with no password", async t => {
      await t
          .setTestSpeed(speed)
          .click('#drawer')
          .typeText('.login_mail', 'lucas.depret@eilwo.zyx')
          .click('#login_submit')
  });

 test("Login with bad password", async t => {
     await t
         .setTestSpeed(speed)
         .click('#drawer')
         .typeText('.login_mail', 'lucas.depret@eilwo.zyx')
         .typeText('.login_secret', 'Please')
         .click('#login_submit')
 });


 test("Login with unavailable account", async t => {
     await t
         .useRole(AccountUser)
 });

test("Signin with available account", async t => {
    await t
    .setTestSpeed(speed)
    .click('#drawer')
    .typeText('.login_mail', 'lucas.depret@eilwo.zyx')
    .typeText('.login_secret', 'Please1coofee')
    .click('#login_signin')
    .navigateTo(`${url}${uriHome}`)
});

test("Signout with available account", async t => {
    await t
        .navigateTo(`${url}${uriHome}`)
        .click('#drawer')
        .click('#login_signout')
        .wait(500)
});