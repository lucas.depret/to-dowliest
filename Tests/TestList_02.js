import { Selector, Role } from "testcafe";
import { domainPort, url } from '../config/test';

const uriLogin = '/';
const uriHome = '/todo';

const speed = 0.3;

const loginErrorMessage = 'Please input your login or email !';
const passwordErrorMessage = 'Please input your password !';
const loginFailedMessage = 'Authentification failed !';

fixture`Todo`.page`${domainPort}${uriLogin}`;

const AccountUser = Role(`${url}${uriLogin}`, async t => {
    await t
        .setTestSpeed(speed)
        .click('#drawer')
        .typeText('.login_mail', 'lucas.depret@eilwo.zyx')
        .typeText('.login_secret', 'Please1coofee')
        .click('#login_submit')
    }
);

  test("Add todo", async t => {
      await t
            .useRole(AccountUser)
            .wait(1000)
            .click('.btn-add')
            .typeText('.addelement', 'Aller chercher du lait')
            .click('.btn-add-include')
            .wait(1000)

  });

  test("Modify todo", async t => {
    await t
        .useRole(AccountUser)
        .wait(1000)
        .click('.expansion-todo')
        .click('.text-wrap')
        .typeText('.input-detail','Avec du Fromage')
        .click('.btn-add')
        .wait(500)
});

test("Delete todo", async t => {
    await t
        .useRole(AccountUser)
        .wait(1000)
        .click('.bg-checkbox-secondary')
        .wait(1000)
});