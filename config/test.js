const domain = 'localhost';
const port = 8080;
const domainPort = `${domain}:${port}`;
const url = `http://${domainPort}`;

export {
  domain,
  port,
  domainPort,
  url,
};
