# To Dowliest

Test d’aptitude javascript frontend proposé par Owlie consistant à la création d'une To do list au format SPA avec les technologies Quasar framework, vuejs, firebase.


## Exemple du projet
Vous pouvez consulter le projet deployé à cette adresse : https://quasar-928bb.web.app/

## Installer les dependences
```bash
npm install
```

### Lancer l'app en mode developpement (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### avec cross-env pour firebase
```bash
npm install --save-dev cross-env
quasar ext add @quasar/qenv
```
Dans le fichier `.quasar.env.json` Il faut mettre votre configuration firebase dans un objet avec pour key "firebaseConfig" sous l'objet principal "developpement".
Vous pouvez ensuite le lancer avec la commande :
```bash
cross-env QENV=development quasar dev
```
ou avec npm
```bash
npm run dev
```

### Lint les fichiers
```bash
npm run lint
```

### Test
Des tests fonctionnels sont réalisés avec Testcafe, pour les exécutés lancez le mode développement de l'application puis:
```bash
npm run testcafe
```

### Build l'app pour production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
